#include <string>
#include <iostream>
#include <vector>
#include <unordered_set>
#include <algorithm>
#include <unordered_map>

using namespace std;

class Solution {
public:
    int numIslands(vector<vector<char>>& grid) {
        int num_island = 0;
        for (int row = 0; row < grid.size(); row++) {
            for (int col = 0; col < grid[row].size(); col++) {
                if (grid[row][col] == '1') {
                    num_island++;
                    dfs(grid, row, col);
                }
            }
        }
        return num_island;
    }
    void dfs(vector<vector<char>> &grid, int row, int col) {
        int max_row = grid.size();
        int max_col = grid[0].size();
        
        if (row >= max_row || row < 0) return;
        if (col >= max_col || col < 0) return;
        if (grid[row][col] != '1') return;
        
        grid[row][col] = 'v';
        
        dfs(grid,row-1,col);
        dfs(grid,row,col-1);
        dfs(grid,row+1,col);
        dfs(grid,row,col+1);
    }
};

int main() {
    Solution s;
    vector<vector<char>> grid({{'1','0','1','1','1'},{'1','0','1','0','1'},{'1','1','1','0','1'}});
    cout << s.numIslands(grid) << endl;
        for (int row = 0; row < grid.size(); row++) {
            for (int col = 0; col < grid[row].size(); col++) {
                cout << grid[row][col] << " ";
            }
            cout << endl;
        }
}
