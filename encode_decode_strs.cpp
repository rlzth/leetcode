#include <string>
#include <iostream>
#include <vector>

using namespace std;

class Codec {
public:

    // Encodes a list of strings to a single string.
    string encode(vector<string>& strs) {
        string encoded_str;
        for (auto str: strs) {
            encoded_str.append(to_string(str.size()));
    	    encoded_str.push_back('\0');
            encoded_str.append(str);
        }
        return encoded_str;
    }

    // Decodes a single string to a list of strings.
    vector<string> decode(string s) {
        vector<string> strs;
        size_t index = 0, start_index = 0;
        while (1) {
            index = s.find('\0', start_index+1);
            if (index == string::npos) break;
            //cout << start_index << " " << index << endl;
            int size = stoi(s.substr(start_index, index-start_index));
            //cout << size << endl;
            strs.push_back(s.substr(index+1, size));
            start_index = index + 1 + size;
        }
        return strs;
    }
};

int main() {
    Codec codec;
    vector<string> strs({"it", "is", "a", "test"}), strs2;
    string encode_str = codec.encode(strs);
    cout << encode_str << endl;
    if (encode_str[1] == '\0') {
    //    cout << "NULL" << endl;
    }
    strs2 = codec.decode(encode_str);
    for (auto i: strs2)	    
        cout << i << endl;
}
// Your Codec object will be instantiated and called as such:
// Codec codec;
// codec.decode(codec.encode(strs));
