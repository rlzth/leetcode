#include <string>
#include <iostream>
#include <vector>
#include <unordered_set>
#include <algorithm>
#include <unordered_map>

using namespace std;

class ValidWordAbbr {
public:
    ValidWordAbbr(vector<string> &dictionary) {
        for (auto word: dictionary) {
            if (word.size() >= 1) {
                string abbr = word2abbr(word);
                if (dict.count(abbr)) {
                    dict[abbr]++;
                } else {
                    dict[abbr] = 1;
                    orig_dict.insert(word);
                }
            }
        }
        //for (auto it = dict.begin(); it != dict.end(); it++) {
        //    cout << it->first<<" "<< it->second<< endl;
        //}
        //for (auto it = orig_dict.begin(); it != orig_dict.end(); it++) {
        //    cout << *it<< endl;
        //}
    }

    bool isUnique(string word) {
        string abbr = word2abbr(word);
        if (!dict.count(abbr)) {
            return true;
        } else if(dict[abbr] == 1 && orig_dict.count(word)){
            //cout << word << " " << orig_dict.count(word) << endl;
            return true;
        }
        return false;
    }

private:
    string word2abbr(string word) {
        if(word.size() <= 1) return word;
        int mid_num = word.size()-2;
        return (word[0] + (mid_num?to_string(mid_num):"") + word[word.size()-1]);
    }
    unordered_map<string,int> dict;
    unordered_set<string> orig_dict;
};

int main() {
    vector<string> dict = {"hello","cat"};
    ValidWordAbbr s(dict);
    cout << (s.isUnique("c1t")?"unique":"no") <<endl;
    cout << (s.isUnique("hello")?"unique":"no") <<endl;
    return 0;
}
