#include <string>
#include <iostream>
#include <vector>
#include <unordered_set>
#include <algorithm>
#include <unordered_map>
#include <cstring>

using namespace std;

int counter = 0;
// Forward declaration of the read4 API.
int read4(char *buf){
    if(!counter){
        counter++;
        memcpy(buf,"ab",2);
        return 2;
    }
    return 0;
};

class Solution {
public:
    /**
     * @param buf Destination buffer
     * @param n   Maximum number of characters to read
     * @return    The number of characters read
     */
    int read(char *buf, int n) {
        int size = 0, t_size = 0;
        if (prev_size) {
            t_size = prev_size;
            memcpy(buf, prev_read, t_size);
        }
        while(t_size < n && (size = read4(buf+t_size))) {
            //cout << "size "<<size << endl;
            t_size += size;
        }
        if (t_size > n) {
            prev_size = t_size-n;
            t_size = n;
            //cout << prev_size << endl;
            memcpy(prev_read, buf+n, prev_size);
        } else {
            prev_size = 0;
        }
        buf[t_size] = '\0';
        return t_size;
    }
private:
    int prev_size=0;
    char prev_read[4];
};

int main() {
    Solution s;
    char buff[4];
    cout << s.read(buff,1) << " ";
    cout << buff << endl;
    cout << s.read(buff,2) << " ";
    cout << buff << endl;
}
